# juniper-backup
This repo provides playbooks for backing up Juniper router configs to a remote server. This demo is using our internal Ansible Automation Platform demo environment. The AAP Controller uses an Execution Environment (EE) ti run the Ansible Core Engine. In this demo we copy the files from the EE to the remote server.

## Getting started

AAP Controller

- [1] Run the Juniper-Rtr-On job-template to turn-on the juniper router (A scheduler will power-offthe router in the evening)
- [2] Run the juniper-backups job-template to copy the backup file to the remote server. The playbook will also list the files in the Juniper_backups directory and prin the current router backup as output.
- [3] Run the remove-juniper-backups job-template to delete the router configs from the remote server



